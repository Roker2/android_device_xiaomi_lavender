Copyright (C) 2019 - kirillmaster4

## Specifications

| Feature                 | Specification                                            |
| :---------------------- | :------------------------------------------------------- |
| CPU                     | Octa-core (4x2.2 GHz Kryo 260 & 4x1.8 GHz Kryo 260)      |
| Chipset                 | Qualcomm SDM660 Snapdragon 660 (14 nm)                   |
| GPU                     | Adreno 512                                               |
| Memory                  | 3/4/6 GB                                                   |
| Shipped Android Version | 9.0.0                                                    |
| Storage                 | 32/64/128 GB                                             |
| MicroSD                 | Up to 256 GB                                             |
| Battery                 | 4000 mAh Li-Po (non-removable)                           |
| Dimensions              | 159.2 x 75.2 x 8.1 mm                                    |
| Display                 | 1080 x 2340 pixels, 6.3 (~409 PPI)                       |
| Rear Camera             | 48 MP, f/1.8, (wide), 1/2", 0.8µm, PDAF                  |
| Front Camera            | 5 MP, f/2.2, depth sens                                  |
| Release Date            | January 2019                                             |

## Device Picture

![Redmi Note 7](https://i.imgur.com/5u4TX5T.jpg "Redmi Note 7")

